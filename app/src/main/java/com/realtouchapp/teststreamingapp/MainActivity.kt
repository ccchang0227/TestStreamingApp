package com.realtouchapp.teststreamingapp

import android.Manifest
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.webkit.PermissionRequest
import com.realtouchapp.teststreamingapp.zixionair.BUNDLE_KEY_BUNDLE
import com.realtouchapp.teststreamingapp.zixionair.BUNDLE_KEY_SERVER_URL
import com.realtouchapp.teststreamingapp.zixionair.BUNDLE_KEY_STREAM_NAME
import com.realtouchapp.teststreamingapp.zixionair.ZixiOnAirActivity
import kotlinx.android.synthetic.main.activity_main.*
import pub.devrel.easypermissions.EasyPermissions

class MainActivity : AppCompatActivity(), EasyPermissions.PermissionCallbacks {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

//        et_server_url.setText("rtmp://52.78.105.136:1935/Live")
//        et_stream_name.setText("a")

//        et_server_url.setText("rtmp://live-tpe03.twitch.tv/app/")
//        et_stream_name.setText("live_62256141_R3MsMM9dJ8bxwIrSFLqDbVJAIwx6Y2")

        et_server_url.setText("rtmps://live-api-s.facebook.com:443/rtmp/")
        et_stream_name.setText("10206818120488872?s_bl=1&s_ps=1&s_sml=3&s_sw=0&s_vt=api-s&a=AbyL0G6Syed4j41i")

        btn_zixi_on_air.setOnClickListener {

            checkPermissions(permissionRequestCodeZixi)

        }

    }

    private fun enterNextActivity(requestCode: Int) {
        val serverURL = et_server_url.text
        val streamName = et_stream_name.text



        when (requestCode) {
            permissionRequestCodeZixi -> {
                val bundle = Bundle()
                bundle.putString(BUNDLE_KEY_SERVER_URL, serverURL.toString())
                bundle.putString(BUNDLE_KEY_STREAM_NAME, streamName.toString())

                val intent = Intent(this@MainActivity, ZixiOnAirActivity::class.java)
                intent.putExtra(BUNDLE_KEY_BUNDLE, bundle)
                startActivity(intent)

            }
        }
    }

    // MARK: - Permissions

    private val permissionRequestCodeZixi = 123

    private fun checkPermissions(requestCode: Int) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            enterNextActivity(requestCode)
            return
        }

        if (!EasyPermissions.hasPermissions(this, Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO, Manifest.permission.INTERNET)) {
            EasyPermissions.requestPermissions(this,
                "Please allow permissions",
                requestCode,
                Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO, Manifest.permission.INTERNET)
        }
        else {
            enterNextActivity(requestCode)
        }

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>?) {
        enterNextActivity(requestCode)
    }

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>?) {
        AlertDialog.Builder(this)
            .setMessage("We are sorry, but app can\\\\'t broadcasting without requested permissions")
            .setCancelable(false)
            .setPositiveButton("", { dialogInterface, i ->
//                finish()
            })
            .show()
    }

}
