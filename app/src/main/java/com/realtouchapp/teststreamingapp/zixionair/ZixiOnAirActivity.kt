package com.realtouchapp.teststreamingapp.zixionair

import android.os.Bundle
import android.util.Log
import android.view.*
import android.view.GestureDetector.SimpleOnGestureListener
import android.view.View.OnTouchListener
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.realtouchapp.teststreamingapp.R
import com.zixi.onairsdk.ZixiConnectionStatistics
import com.zixi.onairsdk.ZixiError
import com.zixi.onairsdk.ZixiOnAirSdk
import com.zixi.onairsdk.camera.ZixiCamera
import com.zixi.onairsdk.camera.ZixiCameraCaps
import com.zixi.onairsdk.camera.ZixiCameraPreset
import com.zixi.onairsdk.events.*
import com.zixi.onairsdk.preview.ZixiOnAirPreview
import com.zixi.onairsdk.settings.ProtocolSettings
import com.zixi.onairsdk.settings.VideoSettings
import com.zixi.onairsdk.settings.ZixiSettings
import kotlinx.android.synthetic.main.activity_zixi_on_air.*

const val BUNDLE_KEY_BUNDLE = "BUNDLE_KEY_BUNDLE"
const val BUNDLE_KEY_SERVER_URL = "BUNDLE_KEY_SERVER_URL"
const val BUNDLE_KEY_STREAM_NAME = "BUNDLE_KEY_STREAM_NAME"

class ZixiOnAirActivity : AppCompatActivity() {
    private val TAG = "ZixiOnAirActivity"

    private class SingleTapConfirm : SimpleOnGestureListener() {
        override fun onSingleTapUp(event: MotionEvent): Boolean {
            return true
        }
    }
    private val mTapDetector = SingleTapConfirm()

    private enum class ContextMenuType {
        NONE, QUALITY
    }
    private var contextMenuType: ContextMenuType = ContextMenuType.NONE

    private var serverURL: String? = null
    private var streamName: String? = null

    private lateinit var mSdk: ZixiOnAirSdk

    // Init states
    private var mUiCreated = false
    private var mSentUiToSdk = false
    private var mZixiReady = false
    private var mCropPreviewMode = false // Default is fit

    private var mCameraFacing = ZixiCameraCaps.CAMERA_FACING_BACK
    private var mCameraRotation = ZixiOnAirPreview.ROTATE_0_DEGREES
    private var mVideoPreset = VideoSettings.ZixiFrameSizePreset1280x720x30

    // Totals of this run
    private var mEncodedVideo: Long = 0 // Encoded video frames
    private var mEncodedAudio: Long = 0 // Encoded audio frames
    private var mTotalAudio: Long = 0 // total video bitstream size in bytes
    private var mTotalVideo: Long = 0 // total audio bitstream size in bytes

    // Ui view measures
    private var mSurfaceW = 0
    private var mSurfaceH = 0

    // Toggle between setting mode 1 and 2
    private var mSettingsToggle = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_zixi_on_air)

        serverURL = intent.getBundleExtra(BUNDLE_KEY_BUNDLE)?.get(BUNDLE_KEY_SERVER_URL) as String?
        streamName = intent.getBundleExtra(BUNDLE_KEY_BUNDLE)?.get(BUNDLE_KEY_STREAM_NAME) as String?
        Log.e(TAG, "serverURL = " + serverURL)
        Log.e(TAG, "streamName = " + streamName)

        btn_fit_crop.setOnClickListener(onClickFitCrop)
        btn_rotate_cam.setOnClickListener(onClickRotate)
        btn_quality.setOnClickListener(onClickVideoQuality)
        btn_mute.setOnClickListener(onClickMute)
        btn_switch_cam.setOnClickListener(onClickSwitchCamera)
        btn_start_stop.setOnClickListener(onClickStartStop)

        btn_fit_crop.isEnabled = false
        btn_rotate_cam.isEnabled = false
        btn_quality.isEnabled = false
        btn_mute.isEnabled = false
        btn_switch_cam.isEnabled = false
        btn_start_stop.isEnabled = false

        tv_time.visibility = View.GONE

        registerForContextMenu(btn_quality)

        setStatus(null)
        setInfo(null)

        mSdk = ZixiOnAirSdk(this)

        // Prevent deletion due to WeakReference
        mSdk.setLogCallback(mLogger)
        mSdk.setCameraEventsHandler(mOnAirCameraEvents)
        mSdk.setStatusEventsHandler(mOnAirCallbacks)

//        mSdk.setDisplayPreset(VideoSettings.ZixiFrameSizePreset1920x1080x30, true)

        // mSdk.initialize();
        mSdk.initialize(
            mCameraFacing,
            ZixiOnAirPreview.PREVIEW_MODE_FIT,
            mVideoPreset
        )

        mSdk.setRawFramesEventsHandler(mRawFramesEventsHandler)
        surface_view_camera.holder.addCallback(mSurfaceCallbacks)

        surface_view_camera.isClickable = true
        surface_view_camera.setOnTouchListener(OnTouchListener { v, event ->
            if (mTapDetector.onSingleTapUp(event)) {
                if (event.action == MotionEvent.ACTION_UP) {
                    val x = event.x
                    val y = event.y
                    if (mZixiReady) mSdk.manualFocus(x, y)
                    Log.e(TAG, "Touch [" + x + "x" + y + "]")
                }
                return@OnTouchListener true
            }
            false
        })
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onPause() {
        super.onPause()

        if (mZixiReady && mSdk.connected()) {
            mSdk.stopStreaming()
        }

    }

    override fun onDestroy() {
        super.onDestroy()

        unregisterForContextMenu(btn_quality)

        Log.e(TAG, "onDestroy")
        mSdk.terminate()
    }

    private var contextItems = mutableListOf<Any>()

    override fun onCreateContextMenu(
        menu: ContextMenu?,
        v: View?,
        menuInfo: ContextMenu.ContextMenuInfo?
    ) {

        if (v == null) {
            super.onCreateContextMenu(menu, v, menuInfo)
            return
        }
        if (menu == null) {
            super.onCreateContextMenu(menu, v, menuInfo)
            return
        }

        var i = 0
        contextItems.clear()
        when (v) {
            btn_quality -> {
                val presets = mSdk.supportedPresets
                for (preset in presets) {
                    when (mCameraFacing) {
                        ZixiCameraCaps.CAMERA_FACING_BACK -> {
                            if (preset.backCameraInfo != null) {
                                val title = "" + preset.width + "x" + preset.height + ", @" + preset.fps// + ".00"
                                menu.add(Menu.NONE, Menu.FIRST + i, Menu.NONE, title)

                                contextItems.add(preset)
                                i ++
                            }
                        }
                        ZixiCameraCaps.CAMERA_FACING_FORWARD -> {
                            if (preset.frontCameraInfo != null) {
                                val title = "" + preset.width + "x" + preset.height + ", @" + preset.fps// + ".00"
                                menu.add(Menu.NONE, Menu.FIRST + i, Menu.NONE, title)

                                contextItems.add(preset)
                                i ++
                            }
                        }
                    }

                }

            }
        }

        menu.add(Menu.NONE, Menu.FIRST+ i, Menu.NONE, "Cancel")

        super.onCreateContextMenu(menu, v, menuInfo)
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        when (contextMenuType) {
            ContextMenuType.QUALITY -> {
                val index = item.itemId - Menu.FIRST
                if (index >= 0 && index < contextItems.size) {
                    val preset = contextItems[index] as ZixiCameraPreset
                    mSdk.selectCameraAndPreset(mCameraFacing, preset.id)

                    mVideoPreset = preset.id
                }
            }
        }

        contextItems.clear()
        contextMenuType = ContextMenuType.NONE

        return super.onContextItemSelected(item)
    }

    // MARK: -

    private fun selectDefaultPreset(facing: Int = ZixiCameraCaps.CAMERA_FACING_FORWARD): ZixiCameraPreset? {
        val presets = mSdk.supportedPresets
        for (preset in presets) {
            Log.e(TAG, "preset = " + preset.id + ", " + preset.name + ", " + preset.backCameraInfo + ", " + preset.frontCameraInfo)
        }

        return null
    }

    // MARK: -

    private val onClickFitCrop = View.OnClickListener {
        mCropPreviewMode = !mCropPreviewMode
        if (mCropPreviewMode) {
            mSdk.previewMode = ZixiOnAirPreview.PREVIEW_MODE_CROP
        } else {
            mSdk.previewMode = ZixiOnAirPreview.PREVIEW_MODE_FIT
        }
    }

    private val onClickRotate = View.OnClickListener {
        when (mCameraRotation) {
            ZixiOnAirPreview.ROTATE_0_DEGREES -> mCameraRotation =
                ZixiOnAirPreview.ROTATE_90_DEGREES
            ZixiOnAirPreview.ROTATE_90_DEGREES -> mCameraRotation =
                ZixiOnAirPreview.ROTATE_180_DEGREES
            ZixiOnAirPreview.ROTATE_180_DEGREES -> mCameraRotation =
                ZixiOnAirPreview.ROTATE_270_DEGREES
            ZixiOnAirPreview.ROTATE_270_DEGREES -> mCameraRotation =
                ZixiOnAirPreview.ROTATE_0_DEGREES
        }

        mSdk.setPreviewCameraRotation(mCameraRotation)

    }

    private val onClickVideoQuality = View.OnClickListener {
        contextMenuType = ContextMenuType.QUALITY

        openContextMenu(it)

    }

    private val onClickMute = View.OnClickListener {
        val ret = mSdk.muteAudio(!mSdk.isAudioMuted)
        if (ret != 0) {
            Log.e(
                TAG,
                "toggleCamera -> " + ZixiError.formatToError(ret)
            )
        }
        else {
            if (mSdk.isAudioMuted) {
                btn_mute.text = "Unmute"
            }
            else {
                btn_mute.text = "Mute"
            }
        }

    }

    private val onClickSwitchCamera = View.OnClickListener {
        if (mSdk.haveTwoCameras()) {
            Log.e(TAG, "toggleCamera")
            val ret = mSdk.switchCamera()
            if (ret != 0) {
                Log.e(
                    TAG,
                    "toggleCamera -> " + ZixiError.formatToError(ret)
                )
            }
            else {
                when (mCameraFacing) {
                    ZixiCameraCaps.CAMERA_FACING_BACK -> {
                        mCameraFacing = ZixiCameraCaps.CAMERA_FACING_FORWARD
                    }
                    ZixiCameraCaps.CAMERA_FACING_FORWARD -> {
                        mCameraFacing = ZixiCameraCaps.CAMERA_FACING_BACK
                    }
                }
            }
        }
    }

    private val onClickStartStop = View.OnClickListener {
        Log.e(TAG, "toggleOnClick")
        if (!mSdk.canConnect()) {
            Toast.makeText(this@ZixiOnAirActivity, "Start failed", Toast.LENGTH_SHORT).show()

            return@OnClickListener
        }

        Log.e(TAG, "toggleOnClick - can connect")
        if (mSdk.connected()) {
            Log.e(TAG, "toggleOnClick - disconnecting")
            mSdk.stopStreaming()

            btn_switch_cam.isEnabled = false
            btn_start_stop.isEnabled = false
        }
        else {
            val settings = ZixiSettings()
            /*
                default -   false.  Encoder will use preset to create a width X height video
                            true.   Encoder will use preset to create a height X width video
            */
//                settings.advanced.verticalOrientation = true;
//                settings.server.rtmpFwd = null;
//                settings.server.password = "";
//                settings.server.channelName = BROADCASTER_CHANNEL_NAME;
//                settings.server.hostName = BROADCASTER_HOST_NAME;
//                settings.protocol.protocolId = ProtocolSettings.PROTCOL_ZIXI;
//                settings.server.bonding = false;


            /* RTMP CONNECTION */
            settings.protocol.protocolId = ProtocolSettings.PROTCOL_RTMP
            settings.rtmp.streamName = streamName
            settings.rtmp.URL = serverURL
            settings.rtmp.password = null
            settings.rtmp.username = null
            //*/
            settings.video.encoderType = VideoSettings.VIDEO_ENCODER_H264
//            settings.video.encoderType = VideoSettings.VIDEO_ENCODER_HEVC
            settings.advanced.verticalOrientation = true
            if (mSettingsToggle) {
                settings.video.frameSizePreset = VideoSettings.ZixiFrameSizePreset1920x1080x30
            } else {
//                settings.video.frameSizePreset = VideoSettings.ZixiFrameSizePreset1280x720x30
//                settings.video.frameSizePreset = VideoSettings.ZixiFrameSizePreset320x240x15;
                settings.video.frameSizePreset = mVideoPreset
            }

            mSdk.setEncoderCameraRotation(mCameraRotation)
//            mSdk.setEncoderCameraRotation(ZixiOnAirPreview.ROTATE_270_DEGREES)

            // File upload - for file transfer application must have read permissions of the file
            //               for store and forward the application must also have write permissions
            //                  to save the data.
//            settings.server.fileSettings = new FileUploadSettings();
//            settings.server.fileSettings.storeAndForward = true/false; true  -> store and forward
//                                                                        false -> file transfer
//            settings.server.fileSettings.localFileName = <FULL_FILE_PATH>;
//            settings.server.fileSettings.overwrite = true/false; true -> if bx have a file named like localFileName it will be overwritten
//                                                                  false -> if bx have a file named like localFileName connect will fail
//            settings.server.fileSettings.remoteLocation = <PATH> ; where to store the uploaded file (path)
            mSdk.startStreamingWithSettings(settings)
            // Rotate the camera to encoder frame
            // mSdk.setEncoderCameraRotation(ZixiOnAirPreview.ROTATE_180_DEGREES);
            // In case of store and forward, when wishing to cause the sdk to start cleaning up the
            // file and stopping the recording call
            // ZixiOnAirSdk.getInstance().finalizeFile();
            // when the ZixiOnAirStatusEvents.zixiOnAirFileFinalized event will be called,
            // its after all the sources have been stopped, encoders wrapped, and the file has
            // been stored. When ZixiOnAirStatusEvents.zixiFileTransferComplete event is fired.
            // All data has also been sent.
            Log.e(TAG, "toggleOnClick - connecting")

            btn_rotate_cam.isEnabled = false
            btn_switch_cam.isEnabled = false
            btn_quality.isEnabled = false
            btn_start_stop.isEnabled = false
        }
        Log.e(TAG, "toggleOnClick - done")

    }

    // MARK: -

    private fun setStatus(status: String?) {
        runOnUiThread {
            tv_status.text = status
        }
    }

    private fun setInfo(info: String?) {
        runOnUiThread {
            tv_info.text = info
        }
    }

    private val mStartZixiThread = Thread(Runnable {
        mSdk.setStatusEventsHandler(mOnAirCallbacks)
        mSdk.setEncodedFramesEventsHandler(mEncodedFramesCallbacks)
        mZixiReady = true
        if (mUiCreated) {
            selectDefaultPreset()

            mSdk.setUiHolder(surface_view_camera.holder)
        }
    })

    private val mSurfaceCallbacks: SurfaceHolder.Callback = object : SurfaceHolder.Callback {
        override fun surfaceCreated(holder: SurfaceHolder) {
            if (mZixiReady) {
                if (!mSentUiToSdk) {

                    selectDefaultPreset()

                    mSdk.setUiHolder(surface_view_camera.holder)
                    mSentUiToSdk = true
                    Log.e(TAG, "surfaceCreated")
                }
            } else {
                mUiCreated = true
            }
        }

        override fun surfaceChanged(
            holder: SurfaceHolder,
            format: Int,
            width: Int,
            height: Int
        ) {
            mSurfaceH = height
            mSurfaceW = width
            if (mZixiReady) {
                Log.e(
                    TAG,
                    "surfaceChanged [" + width + "x" + height + "] " + resources.configuration.orientation
                )
                // ZixiOnAirSdk.getInstance().cameraViewChanged(width,height,getResources().getConfiguration().orientation);
            }
        }

        override fun surfaceDestroyed(holder: SurfaceHolder) {
            mUiCreated = false
            if (mZixiReady) {
                if (mSentUiToSdk) {
                    Log.e(TAG, "surfaceDestroyed")
                    mSdk.setUiHolder(null)
                    mSentUiToSdk = false
                }
            }
        }
    }

    // MARK: -

    private val mLogger = ZixiLogEvents { level, who, what ->
        Log.println(level, "ZixiOnAirSdk", "$who::$what")
    }

    private val mRawFramesEventsHandler: ZixiOnAirRawFramesEvents = object :
        ZixiOnAirRawFramesEvents {
        override fun onRawAudio(bytes: ByteArray, i: Int, l: Long) {}
        private var screenshotNr = 0
        override fun onRawVideo(bytes: ByteArray) {
            screenshotNr ++
            Log.e("RawVideo", "Screenshot #$screenshotNr")
        }
    }

    // Log callback
    private val mLogCallback = ZixiLogEvents { level, who, what ->
        Log.println(level, who, what)
    }

    // Sdk connectivity events
    private val mOnAirCallbacks: ZixiOnAirStatusEvents = object :
        ZixiOnAirStatusEvents {
        override fun zixiOnAirSdkInitialized() {
            setStatus("Initialized")
            mStartZixiThread.start()

            runOnUiThread {
                btn_fit_crop.isEnabled = true
                btn_rotate_cam.isEnabled = true
                btn_quality.isEnabled = true
                btn_mute.isEnabled = true
                btn_switch_cam.isEnabled = true
                btn_start_stop.isEnabled = true

                if (!mSdk.haveTwoCameras()) {
                    btn_switch_cam.visibility = View.GONE
                }
                else {
                    btn_switch_cam.visibility = View.VISIBLE
                }
            }
        }

        override fun zixiOnAirWillStart() {
            setStatus("Will Start")
        }

        override fun zixiOnAirDidStart() {
            setStatus("Did Start")

            runOnUiThread {
                btn_quality.visibility = View.GONE
                btn_quality.isEnabled = true
                btn_rotate_cam.visibility = View.GONE
                btn_rotate_cam.isEnabled = true
                btn_switch_cam.isEnabled = true
                btn_start_stop.isEnabled = true
                btn_start_stop.text = "Stop Streaming"
            }

        }

        override fun zixiOnAirFailedToStart(error: Int) {
            setStatus("Failed To Start " + error + " - " + ZixiError.toString(error))

            runOnUiThread {
                btn_start_stop.isEnabled = true
                btn_quality.isEnabled = true
                btn_rotate_cam.isEnabled = true
                btn_switch_cam.isEnabled = true
            }
        }

        override fun zixiOnAirDidFinish(error: Int) {
            setStatus("Did Finish")
            mSettingsToggle = !mSettingsToggle

            runOnUiThread {
                btn_quality.visibility = View.VISIBLE
                btn_quality.isEnabled = true
                btn_rotate_cam.visibility = View.VISIBLE
                btn_rotate_cam.isEnabled = true
                btn_switch_cam.isEnabled = true
                btn_start_stop.isEnabled = true
                btn_start_stop.text = "Start Streaming"
            }
        }

        override fun zixiOnAirStatistics(o: Any?) {
            if (o == null) {
                return
            }

            if (o is ZixiConnectionStatistics) {
                Log.e(
                    TAG,
                    "zixiOnAirStatistics zixi" + o.bitrate
                )
            } /*else if (o instanceof ZixiRtmpStatistics) {
                ZixiRtmpStatistics r = (ZixiRtmpStatistics)o;
                Log.e(TAG,"zixiOnAirStatistics rtmp" + r.bitrate);
            } */ else Log.e(TAG, "zixiOnAirStatistics")
        }

        override fun zixiOnAirCaptureInfo(width: Int, height: Int, fps: Float) {
            setInfo("" + width + "x" + height + ", @" + String.format("%.02f", fps))
            Log.e(
                TAG,
                "zixiOnAirCaptureInfo [" + width + "x" + height + "]@" + String.format(
                    "%.02f",
                    fps
                )
            )
        }

        // Called whenever video encoder bitrate changes (adaptive = true)
        override fun zixiOnAirVideoEncoderBitrateSet(
            set_bitrate: Int,
            requested_bitrate: Int
        ) {
            Log.e(
                TAG,
                String.format(
                    "zixiOnAirVideoEncoderBitrateSet: %d Quality: %.02f",
                    set_bitrate,
                    set_bitrate.toFloat() / requested_bitrate * 100
                )
            )
        }
        // New events
        /**
         * Store and forward file has finished write on disk, and now continues to be sent over net
         * @param file_size     - final file size on the device
         */
        override fun zixiOnAirFileFinalized(file_size: Long) {}

        /**
         * File upload has finished (store and forward/file transfer)
         * @param transmitted - in bytes, sent in net
         */
        override fun zixiFileTransferComplete(transmitted: Long) {}

        /**
         * connection has been restored
         */
        override fun zixiOnAirReconnected() {
            setStatus("zixiOnAirReconnected")
        }

        /**
         * lost connection to the server
         */
        override fun zixiOnAirConnectivityLost() {
            setStatus("zixiOnAirConnectivityLost")

            mSettingsToggle = false

            runOnUiThread {
                btn_quality.visibility = View.VISIBLE
                btn_quality.isEnabled = true
                btn_rotate_cam.visibility = View.VISIBLE
                btn_rotate_cam.isEnabled = true
                btn_switch_cam.isEnabled = true
                btn_start_stop.isEnabled = true
                btn_start_stop.text = "Start Streaming"
            }
        } // New events end

    }

    private val mOnAirCameraEvents: ZixiOnAirCameraEvents = object :
        ZixiOnAirCameraEvents {
        override fun onManualFocusEnded(succeeded: Boolean) {
            if (succeeded) Log.e(
                TAG,
                "Manual Focus -> ok ? YES"
            ) else Log.e(TAG, "Manual Focus -> ok ? NO")
        }

        override fun onConnectedToCamera(zixiCamera: ZixiCamera) {
            Log.e(TAG, "onConnectedToCamera: " + zixiCamera)
        }

        override fun onDisconnectedFromCamera(zixiCamera: ZixiCamera) {
            Log.e(TAG, "onDisconnectedFromCamera: " + zixiCamera)
        }
    }

    // Encoded frames callbacks
    private val mEncodedFramesCallbacks: ZixiOnAirEncodedFramesEvents = object :
        ZixiOnAirEncodedFramesEvents {
        override fun onEncodedAudio(data: ByteArray, size: Int, pts: Long) {
            mEncodedAudio ++
            mTotalAudio += size.toLong()
            if (mEncodedAudio % 100 == 0L) {
                Log.e(
                    TAG,
                    "Total encoded audio frames $mEncodedAudio $mTotalAudio bytes"
                )
            }
        }

        override fun onEncodedVideo(
            data: ByteArray,
            size: Int,
            pts: Long,
            dts: Long
        ) {
            mEncodedVideo ++
            mTotalVideo += size.toLong()
            if (mEncodedVideo % 100 == 0L) {
                Log.e(
                    TAG,
                    "Total encoded video frames $mEncodedVideo $mTotalVideo bytes"
                )
            }
        }
    }

}
